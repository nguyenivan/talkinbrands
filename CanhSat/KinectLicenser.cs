﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KinectMouseControl
{

    public class KinectLicenseException : Exception
    {
        public KinectLicenseException(string message)
            : base(message)
        {

        }
    }

    public class KinectLicenseFileInvalidException : KinectLicenseException
    {
        public KinectLicenseFileInvalidException()
            : base("Invalid license file.")
        {

        }
    }
    public class KinectLicenseFileNotFoundException : KinectLicenseException
    {
        public KinectLicenseFileNotFoundException()
            : base("License file not found.")
        {

        }
    }

    public class KinectLicenseWrongSensorException : KinectLicenseException
    {
        public KinectLicenseWrongSensorException()
            : base("This software is not licensed to work with this sensor.")
        {
        }
    }

    public class KinectLicenser
    {
        private const int SALT_LEVEL = 10;
        private const int HASH_LEVEL = 5;

        //public KinectLicenser()
        //{
        //}

        public static string FileName
        {
            get
            {
                return new string(new char[] { 'l', 'i', 'c', 'e', 'n', 's', 'e', '.', 'd', 'a', 't' });
            }
        }

        public static bool CheckKinectIdFormat(string kinectUniqueId)
        {
            return (!string.IsNullOrWhiteSpace(kinectUniqueId) && kinectUniqueId.Length > 1);
        }


        /// <summary>
        /// Generate fibonacci 
        /// </summary>
        /// <param name="level">Number of sequences</param>
        /// <returns>Concatenated string in reversed order</returns>
        private static string SaltGenerator(int level)
        {
            string series = "";
            int k, f1, f2, f = 0;
            f1 = f2 = 1;
            if (level < 2)
                return level.ToString();
            else
                for (k = 0; k < level; k++)
                {
                    f = f1 + f2;
                    f2 = f1;
                    f1 = f;
                    series = f.ToString() + series;
                }
            return series;
        }

        private static string GetHash(string kinectUniqueId)
        {
            SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();
            byte[] value = Encoding.UTF8.GetBytes(kinectUniqueId + SaltGenerator(SALT_LEVEL));
            for (int i = 0; i < HASH_LEVEL; i++)
            {
                value = provider.ComputeHash(value);
            }
            return Convert.ToBase64String(value);
        }

        /// <summary>
        /// Create license.dat
        /// </summary>
        /// <param name="folder">Folder path</param>
        public static string GetLicense(IList<string> kinectUniqueIdList)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string kinectUniqueId in kinectUniqueIdList)
            {
                string hash = GetHash(kinectUniqueId);
                builder.AppendLine(kinectUniqueId);
                builder.AppendLine(hash);
            }
            return builder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>

        public static void ValidateLicense(string kinectUniqueId, string fileName)
        {
            try
            {
                using (StreamReader reader = new StreamReader(fileName, Encoding.UTF8))
                {
                    string license = reader.ReadToEnd();
                    string[] tokens = license.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length == 0 || tokens.Length % 2 != 0)
                    {
                        throw new KinectLicenseFileInvalidException();
                    }
                    bool foundKinect = false;
                    for (int i = 0; i < tokens.Length; i = i + 2)
                    {
                        if (kinectUniqueId == tokens[i])
                        {
                            foundKinect = true;
                        }
                        if (!CheckKinectIdFormat(tokens[i]))
                        {
                            throw new KinectLicenseFileInvalidException();
                        }
                        if (GetHash(tokens[i]) != tokens[i + 1])
                        {
                            throw new KinectLicenseFileInvalidException();
                        }
                    }
                    if (!foundKinect)
                    {
                        throw new KinectLicenseWrongSensorException();
                    }
                }
            }
            catch (FileNotFoundException)
            {
                throw new KinectLicenseFileNotFoundException();
            }
            catch (FileLoadException)
            {
                throw new KinectLicenseFileNotFoundException();
            }
        }


        ///// <summary>
        ///// Create license.dat
        ///// </summary>
        ///// <param name="folder">Folder path</param>
        //public static async void GenerateLicense(IList<string> kinectUniqueIdList, string folder)
        //{
        //    if (!Directory.Exists(folder))
        //    {
        //        Directory.CreateDirectory(folder);
        //    }
        //    string filePath = Path.Combine(folder, FileName);
        //    using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8))
        //    {
        //        await writer.WriteAsync(GetLicense(kinectUniqueIdList));
        //    }
        //}

        /// <summary>
        /// Create license.dat
        /// </summary>
        /// <param name="folder">Folder path</param>
        public static async void SaveLicense(string license, string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, Encoding.UTF8))
            {
                await writer.WriteAsync(license);
            }
        }




    }
}
