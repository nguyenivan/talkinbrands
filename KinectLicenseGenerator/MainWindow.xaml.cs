﻿using Microsoft.Kinect;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KinectMouseControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }        

        private async void Button_Get(object sender, RoutedEventArgs e)
        {
            try
            {
                KinectSensor kinect = KinectSensor.GetDefault();
                if (kinect != null)
                {
                    kinect.Open();
                    int limitOfTrial = 3;
                    int delayMilliecond = 1000;
                    for (int i = 0; i < limitOfTrial; i++)
                    {
                        if (kinect.IsOpen && !string.IsNullOrWhiteSpace(kinect.UniqueKinectId))
                        {
                            this.TextBoxKinectId.Text = kinect.UniqueKinectId;
                            return;
                        }
                        await Task.Delay(delayMilliecond);
                    }
                }
                throw new Exception("No Kinect detected. Please try to (re)plug your Kinect sensor.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

        }

        private void Button_Generate(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.TextBoxKinectId.Text))
                {
                    throw new Exception("No Kinect ID entered.");
                }
                string[] kinectIdList = this.TextBoxKinectId.Text.Split(new string[] { " ", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                if (kinectIdList.Length == 0)
                {
                    throw new Exception("No Kinect ID entered.");
                }
                foreach (string kinectId in kinectIdList)
                {
                    if (!KinectLicenser.CheckKinectIdFormat(kinectId))
                    {
                        throw new Exception(kinectId + " is not valid Kinect ID.");
                    }
                }

                this.TextBoxKinectLicense.Text = KinectLicenser.GetLicense(kinectIdList);

                this.ButtonSaveLicense.IsEnabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void Button_Save(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
                openFileDialog1.Filter = "dat files (*.dat)|*.dat|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.FileName = KinectLicenser.FileName;
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.CheckFileExists = false;

                bool? dialogValue = openFileDialog1.ShowDialog();

                if (dialogValue.HasValue && dialogValue.Value)
                {
                    KinectLicenser.SaveLicense(TextBoxKinectLicense.Text, openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
